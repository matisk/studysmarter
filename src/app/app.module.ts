import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WeekdaysComponent } from './weekdays/weekdays.component';
import { WeekdayComponent } from './weekdays/weekday/weekday.component';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';

@NgModule({
  declarations: [
    AppComponent,
    WeekdaysComponent,
    WeekdayComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [
    BrowserDynamicTestingModule,
    WeekdaysComponent,
    WeekdayComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
