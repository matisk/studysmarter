import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-weekday',
  templateUrl: './weekday.component.html',
  styleUrls: ['./weekday.component.scss']
})
export class WeekdayComponent implements OnInit {

  @Input() public today = '';
  @Input() public active = false;

  constructor() { }

  ngOnInit(): void {
  }

}
