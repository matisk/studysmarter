import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WeekdaysComponent } from './weekdays.component';

describe('WeekdaysComponent', () => {
  let component: WeekdaysComponent;
  let fixture: ComponentFixture<WeekdaysComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WeekdaysComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WeekdaysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return 6', () => {
    let day = component.whichDay(0)
    expect(day).toEqual(6);
  });

  it('should return day', () => {
    let day = component.whichDay(1)
    expect(day).toEqual(0);
  });
});
