import { Component } from '@angular/core';

@Component({
  selector: 'app-weekdays',
  templateUrl: './weekdays.component.html',
  styleUrls: ['./weekdays.component.scss']
})
export class WeekdaysComponent {

  public date: Date;
  public weekday;
  public daysOfWeek = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];

  constructor() {
    this.date = new Date();
    this.weekday = this.whichDay(this.date.getDay());
  }

  whichDay(day: number) {
    if (day === 0) {
      return 6;
    } else {
      return day - 1;
    }
  }
}
